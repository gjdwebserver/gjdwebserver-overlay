# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit meson gnome2-utils 

DESCRIPTION="Graphical power_supply status tool for Linux mobile platforms"
HOMEPAGE="https://gitlab.com/MartijnBraam/powersupply/"
SRC_URI="https://gitlab.com/MartijnBraam/powersupply/-/archive/${PV}/powersupply-${PV}.tar.gz"

LICENSE="LGPL-2.1+"
SLOT="0"
KEYWORDS="~x86 ~amd64 ~arm64 ~arm"

RDEPEND="
	gui-libs/libhandy
"

DEPEND="
	${RDEPEND}
"		
	
src_configure() {
	meson_src_configure
}

src_compile() {
	meson_src_compile
}

src_install() {
	meson_src_install
}

pkg_postinst() {
	xdg_pkg_postinst
	gnome2_schemas_update
	xdg_icon_cache_update
}

pkg_postrm() {
	xdg_pkg_postrm
	gnome2_schemas_update
	xdg_icon_cache_update
}
