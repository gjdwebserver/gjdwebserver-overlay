# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit gnome2-utils meson xdg

COMMIT="6122062db5bed238cae5e65578470b10ff7ee535"
DESCRIPTION="A GTK3 camera application that knows how to deal with the media request api"
HOMEPAGE="https://gitlab.com/megapixels-org/Megapixels"
SRC_URI="https://gitlab.com/megapixels-org/Megapixels/-/archive/${PV}/${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~arm64"

DEPEND="
	gui-libs/gtk
	x11-libs/gtk+:3
	media-libs/tiff
	media-gfx/zbar
	media-libs/libepoxy
	media-libs/libraw
	media-gfx/dcraw
	media-gfx/imagemagick
	media-gfx/argyllcms
	dev-libs/feedbackd
	media-libs/libmegapixels
	media-libs/postprocessd
"

PATCHES=(
	"${FILESDIR}"/06230f3a02cffdf8b683f85cb32fc256d73615d9.patch
        "${FILESDIR}"/27a1e606d680295e0b4caceadf74ff5857ac16b2.patch
        "${FILESDIR}"/d8b35bc223989cb165ba1b0716ab9f0ca9c43e53.patch
)

RDEPEND="${DEPEND}"
BDEPEND=""

S="${WORKDIR}/Megapixels-${PV}-${COMMIT}"

src_configure() {
	meson_src_configure
}

pkg_postinst() {
	xdg_pkg_postinst
	gnome2_schemas_update
}

pkg_postrm() {
	xdg_pkg_postrm
	gnome2_schemas_update
}
