# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit gnome2-utils meson xdg

COMMIT=a0b1f4377f0560aee34b50bd3edfb8bd2ecb384e

DESCRIPTION="The raw picture to jpg processor"
HOMEPAGE="https://gitlab.com/megapixels-org/postprocessd"
SRC_URI="https://gitlab.com/megapixels-org/postprocessd/-/archive/${COMMIT}/postprocessd-${COMMIT}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~arm64"

S="${WORKDIR}/postprocessd-${COMMIT}"

DEPEND="
	dev-libs/libconfig
	media-libs/opencv[contrib]
	media-libs/libraw
	media-libs/tiff
	media-libs/libjpeg-turbo
	media-libs/exiftool
"

RDEPEND="${DEPEND}"
BDEPEND=""

src_configure() {
	meson_src_configure
}

pkg_postinst() {
	xdg_pkg_postinst
	gnome2_schemas_update
}

pkg_postrm() {
	xdg_pkg_postrm
	gnome2_schemas_update
}
