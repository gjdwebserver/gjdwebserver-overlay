Gentoo overlay: GJDWebserver

This is a personal overlay I use for my computers and my Pine64 PinePhone Pro.


**Binaryhost**

For the PinePhone Pro I've created a binaryhost: https://distfiles.gjdwebserver.nl/

More information on this you can find on my blog: https://blog.gjdwebserver.nl/ords/f?p=107:HOME:::::ARTICLE:gjdwebserver-binaryhost-packages


**Setup**

Add the following content to /etc/portage/repos.conf/gjdwebserver.conf

```
[gjdwebserver]
location = /var/db/repos/gjdwebserver
sync-type = git
sync-uri = https://git.gjdwebserver.nl/gjdwebserver/gjdwebserver-overlay
auto-sync = yes
```

Or run eselect repository add gjdwebserver git https://git.gjdwebserver.nl/gjdwebserver/gjdwebserver-overlay.git
