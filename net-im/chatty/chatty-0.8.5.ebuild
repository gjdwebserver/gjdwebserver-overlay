# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7


inherit meson gnome2-utils xdg
LIBCM_COMMIT="a338cafc420656a30adeb873430e978f0a688a33"
COMMIT="7bea437c9e3086148da41a91e82218c9422aa67f"

DESCRIPTION="XMPP and SMS messaging via libpurple and Modemmanager"
HOMEPAGE="https://gitlab.gnome.org/World/Chatty"
SRC_URI="
	https://source.puri.sm/Librem5/libcmatrix/-/archive/${LIBCM_COMMIT}/libcmatrix-${LIBCM_COMMIT}.tar.gz
	https://gitlab.gnome.org/World/Chatty/-/archive/v${PV}/${PN}-v${PV}.tar.gz
	"

LICENSE="GPL-3"
SLOT="0"
IUSE="spelling"
KEYWORDS="~arm64"

DEPEND="	>=gnome-extra/evolution-data-server-3.54.0[phonenumber]
		dev-libs/feedbackd
		gui-libs/libhandy
		x11-plugins/purple-mm-sms
		dev-libs/olm
		dev-libs/libphonenumber
		x11-libs/gtk+:3
		spelling? ( app-text/libspelling )
"
RDEPEND="${DEPEND}"
BDEPEND="${DEPEND}"

S="${WORKDIR}/Chatty-v${PV}-${COMMIT}"

src_prepare() {
	default
	eapply_user
	rm -r "${S}"/subprojects/libcmatrix.wrap || die
	mv "${WORKDIR}"/libcmatrix-"${LIBCM_COMMIT}" "${S}"/subprojects/libcmatrix || die
}

pkg_postinst() {
	xdg_pkg_postinst
	gnome2_schemas_update
}

pkg_postrm() {
	xdg_pkg_postrm
	gnome2_schemas_update
}
