# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7


inherit meson gnome2-utils xdg
LIBCM_COMMIT="6c260ee37bd2eff096ee44c29690f30718566c1c"
COMMIT="28b23d3d1a376b4691e031010fbe4a4b0da64d69"

DESCRIPTION="XMPP and SMS messaging via libpurple and Modemmanager"
HOMEPAGE="https://gitlab.gnome.org/World/Chatty"
SRC_URI="
	https://source.puri.sm/Librem5/libcmatrix/-/archive/${LIBCM_COMMIT}/libcmatrix-${LIBCM_COMMIT}.tar.gz
	https://gitlab.gnome.org/World/Chatty/-/archive/v${PV}/${PN}-v${PV}.tar.gz
	"

LICENSE="GPL-3"
SLOT="0"
IUSE="spelling"
KEYWORDS="~arm64"

DEPEND="	gnome-extra/evolution-data-server[phonenumber]
		dev-libs/feedbackd
		gui-libs/libhandy
		x11-plugins/purple-mm-sms
		dev-libs/olm
		dev-libs/libphonenumber
		x11-libs/gtk+:3
		spelling? ( app-text/libspelling )
"
RDEPEND="${DEPEND}"
BDEPEND="${DEPEND}"

S="${WORKDIR}/Chatty-v${PV}-${COMMIT}"

src_prepare() {
	default
	eapply_user
	rm -r "${S}"/subprojects/libcmatrix || die
	rm -r "${S}"/subprojects/libcmatrix.wrap || die
	mv "${WORKDIR}"/libcmatrix-"${LIBCM_COMMIT}" "${S}"/subprojects/libcmatrix || die
}

pkg_postinst() {
	xdg_pkg_postinst
	gnome2_schemas_update
}

pkg_postrm() {
	xdg_pkg_postrm
	gnome2_schemas_update
}
